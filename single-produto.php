<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php while(have_posts()): the_post(); ?>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <nav aria-label="breadcrumb" class="d-flex justify-content-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= home_url() ?>">Home</a></li>
            <?php
              $terms = get_the_terms( get_the_ID(), 'categoria' );
              if ( ! empty( $terms ) ):
            ?>
            <li class="breadcrumb-item"><a
                href="<?= esc_url( get_category_link( $terms[0]->term_id ) ) ?>"><?= esc_html( $terms[0]->name ) ?></a>
            </li>
            <?php endif; ?>
            <li class="breadcrumb-item"><?php the_title(); ?></li>
          </ol>
        </nav>
        <div class="row page-top">
          <div class="col-md-12 mx-auto">
            <h1 class="product__heading"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="row product__main">
          <div class="col-md-5 mx-auto">
            <div class="mainImage">
              <img src="<?=
              has_post_thumbnail() ?
              the_post_thumbnail_url('produto') :
              get_template_directory_uri() . '/img/produto-sem-imagem.jpg';
              ?>" class="img-fluid" />
            </div>
          </div>
          <div class="col-md-4 mx-auto">
            <p class="text-center">
              <button onclick="addToCart(<?php the_ID(); ?>);" class="btn btn-outline-primary"><i
                  class="fa fa-shopping-cart"></i>&nbsp;Presupuesto</button>
            </p>
            <div class="product__details">

              <?php 
              
              $blocks = parse_blocks( get_the_content() );

              echo render_block( $blocks[0] );

              ?>

            </div>
            <div class="product__social social">
              <h4>MOSTRALO A TUS AMIGOS</h4>
              <?php echo do_shortcode('[addtoany]'); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 mx-auto">
              <?php

              foreach(array_slice($blocks,1) as $block){
                echo render_block($block);
              }
              
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /container -->
    <?php

			if( comments_open() || get_comments_number()):
				comments_template();
			endif;

		endwhile;

		?>
  </div>

  <?php get_footer(); ?>
