<?php
/**
 * Template Name: Geral
 *
 * Description: Custom page for general use.
 *
 * @package WordPress
 * @subpackage Walder
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php while(have_posts()): the_post(); ?>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <nav aria-label="breadcrumb" class="d-flex justify-content-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= home_url() ?>">Home</a></li>
            <li class="breadcrumb-item"><?php the_title(); ?></li>
          </ol>
        </nav>
        <div class="row page-top">
          <div class="col-md-12 mx-auto">
            <h1 class="product__heading"><?php the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <?php the_content();  ?>
      </div>
    </div>
  </div>
  <!-- /container -->
  <?php

			if( comments_open() || get_comments_number()):
				comments_template();
			endif;

		endwhile;

		?>
</div>

<?php get_footer(); ?>
