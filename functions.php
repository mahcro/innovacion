<?php

// CPTS
require get_template_directory() . '/inc/cpts.php';
// Requerendo os Term Metas
require get_template_directory() . '/inc/terms-meta.php';
// Requerendo o arquivo do Customizer
require get_template_directory() . '/inc/customizer.php';
// Requerendo o arquivo de AJAX
require get_template_directory() . '/inc/ajax-functions.php';

//Remove pages for non-admins
add_action('admin_menu', 'adjust_admin_menu_nonadm', 999);

function adjust_admin_menu_nonadm() {
    if (!current_user_can('manage_options')) {

      // get the the role object
      $editor = get_role('editor');
      // add $cap capability to this role object
      $editor->add_cap('edit_theme_options');
        if (class_exists('Contact')) {
            remove_menu_page('contact');
        }
        remove_menu_page('tools.php'); // Tools
        remove_menu_page('edit.php'); // Posts
        remove_menu_page('edit.php?post_type=page'); // Pages
        remove_menu_page('options-general.php'); // Settings
        remove_menu_page('edit-comments.php'); // Comments
        remove_menu_page( 'wpcf7' ); // CONTACT FORM
        remove_submenu_page( 'themes.php', 'widgets.php' ); // widgets
        remove_submenu_page( 'themes.php', 'themes.php' ); // edit theme



        /*
     remove_menu_page('index.php'); // Dashboard
     remove_menu_page('edit.php'); // Posts
     remove_menu_page('upload.php'); // Media
     remove_menu_page('link-manager.php'); // Links
     remove_menu_page('edit.php?post_type=page'); // Pages
     remove_menu_page('edit-comments.php'); // Comments
     remove_menu_page('themes.php'); // Appearance
     remove_menu_page('plugins.php'); // Plugins
     remove_menu_page('users.php'); // Users
     remove_menu_page('tools.php'); // Tools
     remove_menu_page('options-general.php'); // Settings
    */
    }
}

// Estização de página de login
function login_styles() { ?>
    <style type="text/css">
    .login h1 a {background-size: 250px 150px;width: 250px;height: 150px;background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/logo-morfose.png')}
    </style>
<?php }
add_action('login_head', 'login_styles');

// Link logo login
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

// Mudar nome ao passar o mouse
function my_login_logo_url_title() {
    return 'Innovación';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function contact_message() {
  return '<p style="text-align: center"><strong>Contato:</strong><br>Mateus 67 99615-3429</p>';
}
add_filter('login_message', 'contact_message');

// adicionando classes na li dos menus
function add_additional_class_on_li($classes, $item, $args) {
    if($args->add_li_class) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

// removendo barra de admin
function remove_admin_bar() {
  show_admin_bar(false);
}
//add_action('after_setup_theme', 'remove_admin_bar');

// Carregando css e js
function load_scripts(){

	// Google Fonts -->
  wp_enqueue_style( 'montserrat-font', 'https://fonts.googleapis.com/css?family=Montserrat:400,700' );
  wp_enqueue_style( 'raleway-font', 'https://fonts.googleapis.com/css?family=Raleway:300,400,700' );
	// Library CSS -->
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome-css', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css' );;
	// Theme CSS -->
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/assets/css/style.default.css' );
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/assets/css/custom.css' );

	// The Scripts -->
  wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/vendor/jquery/jquery.min.js', [], '3.3.1' );
  wp_enqueue_script('popper', get_template_directory_uri() . '/assets/vendor/popper.js/umd/popper.min.js' );
  wp_enqueue_script('bootstrap.min.js', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js' );
  wp_enqueue_script('jquery.cookie.js', get_template_directory_uri() . '/assets/vendor/jquery.cookie/jquery.cookie.js' );
  wp_enqueue_script('jquery.front.js', get_template_directory_uri() . '/assets/js/front.js' );
  wp_enqueue_script('custom.js', get_template_directory_uri() . '/assets/js/custom.js' );
  wp_enqueue_script(array('jquery', 'editor', 'thickbox', 'media-upload'));

  wp_localize_script( 'custom.js', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );

}
add_action( 'wp_enqueue_scripts', 'load_scripts' );

function walder_config(){
	// Registrando nossos menus
	register_nav_menus(
	  [
	    'my_main_menu' => 'Menu Principal'
	  ]
	);

	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', [ 'video', 'image' ] );
  add_theme_support( 'title-tag');

  // tamanhos Gutenberg
  add_theme_support( 'align-wide' );

  add_image_size( 'produto', 472, 600, true );
}
add_action( 'after_setup_theme', 'walder_config', 0 );

// Registrando Sidebars
add_action( 'widgets_init', 'walder_sidebars' );
function walder_sidebars(){
  register_sidebar(
		array(
			'name' => 'Footer facebook',
			'id' => 'sidebar-1',
			'description' => 'Espaço para facebook',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => ''
		)
	);

  register_sidebar(
		array(
			'name' => 'Footer Newsletter',
			'id' => 'sidebar-2',
			'description' => 'Espaço para Newsletter',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => ''
		)
	);

  register_sidebar(
		array(
			'name' => 'Footer Contato',
			'id' => 'sidebar-3',
			'description' => 'Espaço para Contatos',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => ''
		)
	);
}

//Custom Admin Bar
add_action('wp_before_admin_bar_render', 'wps_admin_bar');

function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    //$wp_admin_bar->remove_menu('view-site');
    //$wp_admin_bar->remove_menu('edit');
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('comments');

}

function custom_posts_per_page( $query ) {

  if ( $query->is_post_type_archive('produto') ) {
      set_query_var('posts_per_page', -1);
  }
}
add_action( 'pre_get_posts', 'custom_posts_per_page' );


// Modificando o get title
function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'walder_archive_title' );

add_filter( 'wp_list_categories', function( $html ) {
    return str_replace( ' current-cat', ' active', $html );
});

add_action( 'walder_count_cart', 'walder_echo_count_cart' );
function walder_echo_count_cart(){
  if ( isset($_COOKIE['walder_products_in_cart']) ) {
    $qtd = count(unserialize(stripslashes( $_COOKIE['walder_products_in_cart'] )));
    echo "Usted tiene <b id='qtdCart'>$qtd</b> producto(s) en el presupuesto.";
  }
  else{
    echo 'Usted no tiene productos en el presupuesto.';
  }

}

add_action( 'walder_table_cart', 'walder_create_table_cart' );
function walder_create_table_cart(){

  if ( isset($_COOKIE['walder_products_in_cart']) ) {
    $products  = unserialize(stripslashes( $_COOKIE['walder_products_in_cart'] ));

    echo '
    <table id="product-table" class="table">
      <thead>
        <tr>
          <th colspan="2">Producto</th>
          <th>Quantidad</th>
          <th colspan="2"></th>
        </tr>
      </thead>
      <tbody>
    ';

    foreach($products as $product){
      $img = $product['image'] ? $product['image'] : get_template_directory_uri() . '/img/produto-sem-imagem.jpg';
      echo '
      <tr id="product-' . $product['id'] . '">
        <td><img src="' . $img . '"></a></td>
        <td>' . $product['name'] . '</td>
        <td><input id="qt' . $product['id'] . '" data-product-id="' . $product['id'] . '" type="number" value="' . $product['qt'] . '" class="form-control" onblur="saveToDB(' . $product['id'] . ')"></td>
        <td><button id="exclui' . $product['id'] . '" onclick="removeFromCart(' . $product['id'] . ')"><i class="fa fa-trash"></i></button></td>
      </tr>
      ';
    }
    echo '</tbody>
  </table>';
  }
}

add_action( 'walder_cart_form', 'walder_create_cart_form' );
function walder_create_cart_form(){
  if ( isset($_COOKIE['walder_products_in_cart']) ) {
    echo do_shortcode('[contact-form-7 title="Formulário Orçamento"]');
  }
}

/*
    Prevent the email sending step for specific form
*/
add_action("wpcf7_before_send_mail", "wpcf7_add_cart_content");
function wpcf7_add_cart_content($cf7) {

    if ($cf7->title() == "Formulário Orçamento") {
      //Get current form
      $wpcf7      = WPCF7_ContactForm::get_current();

      // get current SUBMISSION instance
      $submission = WPCF7_Submission::get_instance();

      // Ok go forward
      if ($submission) {

          // get submission data
          $data = $submission->get_posted_data();

          // nothing's here... do nothing...
          if (empty($data))
              return;

          // criando tabela de Orçamento
          if ( isset($_COOKIE['walder_products_in_cart']) ) {
            $products  = unserialize(stripslashes( $_COOKIE['walder_products_in_cart'] ));

            $orcamento = '<h3>Datos del presupuesto</h3>';
            $orcamento .= '<table class="table" style="width: 100%; max-width: 800px; border: 1px solid;">
                      		<thead>
                      		<tr>
                      		<th style="text-align: left">Producto</th>
                      		<th style="text-align: left">Cantidad</th>
                      		</tr>
                      		</thead>
                      		<tbody>';

            foreach($products as $product){
              $orcamento .= '<tr>
                            <td>' . $product['name'] . '</td>
                            <td>' . $product['qt'] . '</td>
                            </tr>';
            }

            $orcamento .= '</tbody></table>';
          }
          // do some replacements in the cf7 email body
          $mail         = $wpcf7->prop('mail');

          $mail['body'] = str_replace('[walder-orcamento]', $orcamento, $mail['body']);

          // Save the email body
          $wpcf7->set_properties(array(
              "mail" => $mail
          ));

          // return current cf7 instance
          return $wpcf7;
        }
    }
}

add_action("wpcf7_mail_sent", "wpcf7_clear_cookies");
function wpcf7_clear_cookies($cf7) {
    if ($cf7->title() == "Formulário Orçamento") {
      unset( $_COOKIE['walder_products_in_cart'] );
      setcookie( 'walder_products_in_cart', '', time() - ( 15 * 60 ), '/' );
    }
}

/*
**  return count of post in category child of ID
*/
function wp_get_postcount($id, $taxonomy) {
  $q = new WP_Query(array(
    'nopaging' => true,
    'tax_query' => array(
      array(
                'taxonomy' => $taxonomy,
                'field' => 'term_id',
                'terms' => $id,
                'include_children' => true,
            ),
            'fields' => 'ids',
  )));
  return $q->post_count;
}
