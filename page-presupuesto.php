<?php get_header(); ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="container">
    <div class="col-lg-12">
      <nav aria-label="breadcrumb" class="d-flex justify-content-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?= home_url() ?>">Home</a></li>
          <li class="breadcrumb-item">Presupuesto</li>
        </ol>
      </nav>
      <div class="row page-top">
        <div class="col-md-10 mx-auto">
          <h1>Presupuesto</h1>
          <p class="text-muted"><?php do_action( 'walder_count_cart' ); ?></p>
        </div>
      </div>
    </div>
    <div id="carrinho_orcamento" class="row mb-5">
      <div id="basket" class="col-lg-9">
        <div class="box">

            <div class="table-responsive">
              <?php do_action( 'walder_table_cart' ); ?>
            </div>
            <div class="box-footer d-flex justify-content-between flex-column flex-lg-row">
              <div class="left">
                <a href="<?php echo get_post_type_archive_link( 'produto' ); ?>" class="btn btn-outline-secondary"><i class="fa fa-chevron-left"></i>Seguir presupuestando</a></div>
              <div class="right">
                <button id="btnClearCart" onclick="clearCart()" class="btn btn-outline-primary">Borrar presupuesto</button>
              </div>
            </div>

        </div>
      </div>
      <div id="basket-form" class="col-lg-3 text-center">
        <?php do_action( 'walder_cart_form' ); ?>
      </div>
    </div>
  </div>
</div>
<script>
  var wpcf7Elm = document.querySelector( '.wpcf7' );

  wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {

  jQuery('#carrinho_orcamento').html(
    '<div class=\'col-md-12 col-sm-12 col-lg-12 text-center\'><h1>Gracias</h1> <h2>Su presupuesto fue recibido. En breve le responderemos.</h2></div>');
	jQuery('html, body').animate({
		scrollTop: 0
	}, 800);
  jQuery("#qtdCart").html("0");

  }, false );
</script>

<?php get_footer(); ?>
