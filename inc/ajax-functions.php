<?php

// Actions to addToCart
add_action('wp_ajax_addToCart', 'addToCart');
add_action('wp_ajax_nopriv_addToCart', 'addToCart');

// Actions to clearCart
add_action('wp_ajax_clearCart', 'clearCart');
add_action('wp_ajax_nopriv_clearCart', 'clearCart');

// Actions to removeFromCart
add_action('wp_ajax_removeFromCart', 'removeFromCart');
add_action('wp_ajax_nopriv_removeFromCart', 'removeFromCart');

// Actions to modifyCart
add_action('wp_ajax_modifyCart', 'modifyCart');
add_action('wp_ajax_nopriv_modifyCart', 'modifyCart');

// addToCart
function addToCart() {
  global $wpdb;

  // pegando o produto
  $id = $_POST[ 'id' ];
  $product = get_post( $id );

  $product_id = $id;
  $product_name = get_the_title($product);
  $product_image = get_the_post_thumbnail_url($product);

  if ( isset($_COOKIE['walder_products_in_cart']) ) {

	  $products  = unserialize(stripslashes( $_COOKIE['walder_products_in_cart'] ));

    //verificar se já está no array
    if (!array_key_exists($product_id, $products)) {

      $products[ $id ][ 'id' ] = $product_id;
      $products[ $id ][ 'name' ] = $product_name;
      $products[ $id ][ 'image' ] = $product_image;
      $products[ $id ][ 'qt' ] = 1;

      setcookie( 'walder_products_in_cart', serialize($products), (time() + (1 * 12 * 3600)), '/' );
    }

  } else {

    $products = [];

    $products[ $id ][ 'id' ] = $product_id;
    $products[ $id ][ 'name' ] = $product_name;
    $products[ $id ][ 'image' ] = $product_image;
    $products[ $id ][ 'qt' ] = 1;

    setcookie( 'walder_products_in_cart', serialize($products), (time() + (1 * 12 * 3600)), '/' );
  }

  $response = [
    'message' => "Producto agregado al presupuesto!",
    //'products' => $_COOKIE['walder_products_in_cart'],
    'success' => true,
  ];

  wp_send_json($response);
}

// function clear cart
function clearCart(){
  unset( $_COOKIE['walder_products_in_cart'] );
  setcookie( 'walder_products_in_cart', '', time() - ( 15 * 60 ), '/' );
  //echo $_COOKIE['walder_products_in_cart'];

  echo '<script>
  jQuery("#product-table").hide("500");
  jQuery("#basket-form").hide("500");
  jQuery("#qtdCart").html("0");
  </script>';

  wp_die();
}

function removeFromCart(){

  $id = $_POST[ 'id' ];

  if ( isset($_COOKIE['walder_products_in_cart']) ) {

	  $products = unserialize(stripslashes( $_COOKIE['walder_products_in_cart'] ));

    unset($products[$id]);

		if( setcookie( 'walder_products_in_cart', serialize($products), (time() + (1 * 12 * 3600)), '/' ) ){
			echo '<script>
			jQuery("#product-' . $id . '").hide("500");
      jQuery("#qtdCart").html(" ' .count($products) . '");
			</script>';
		}

  }
  wp_die();
}

function modifyCart(){

  $id = $_POST[ 'id' ];
  $qtd = $_POST[ 'qtd' ];

  if ( isset($_COOKIE['walder_products_in_cart']) ) {

	  $products = unserialize(stripslashes( $_COOKIE['walder_products_in_cart'] ));

    //unset($products[$id]);
    $products[$id]['qt'] = $qtd;

		if( setcookie( 'walder_products_in_cart', serialize($products), (time() + (1 * 12 * 3600)), '/' ) ){
			echo 'ok';
		}

  }
  wp_die();
}
