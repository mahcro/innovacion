<?php

class Cat_Pro_Term_Meta {

	public function __construct() {

		if ( is_admin() ) {

			add_action( 'categoria_add_form_fields',  array( $this, 'create_screen_fields'), 10, 1 );
			add_action( 'categoria_edit_form_fields', array( $this, 'edit_screen_fields' ),  10, 2 );

			add_action( 'created_categoria', array( $this, 'save_data' ), 10, 1 );
			add_action( 'edited_categoria',  array( $this, 'save_data' ), 10, 1 );

      add_action( 'admin_enqueue_scripts', array( $this, 'load_media' ) );
      add_action( 'admin_footer', array ( $this, 'add_script' ) );

		}

	}

	public function create_screen_fields( $taxonomy ) {

		// Form fields.
		echo '<div class="form-field term-cat_pro_featured-wrap">';
		echo '	<label for="cat_pro_featured">' . __( 'Destaque', 'walder' ) . '</label>';
		echo '	<input name="cat_pro_featured" type="checkbox" value="1">';
		echo '	<p class="description">' . __( 'Categoria em destaque na home', 'text_domain' ) . '</p>';
		echo '</div>';

    echo '<div class="form-field term-group">';
    echo ' <label for="category-image-id">' . __('Imagem Destaque', 'walder') . '</label>';
    echo ' <input type="hidden" id="category-image-id" name="category-image-id" class="custom_media_url" value="">';
    echo ' <div id="category-image-wrapper"></div>';
    echo ' <p>';
    echo '   <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="' . __( 'Adicionar Imagem', 'walder' ) . '" />';
    echo '   <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="' . __( 'Remover Imagem', 'walder' ) . '" />';
    echo ' </p>';
    echo '</div>';

	}

	public function edit_screen_fields( $term, $taxonomy ) {

    // Retrieve an existing value from the database.
    $cat_pro_featured = get_term_meta( $term->term_id, 'cat_pro_featured', true );

    echo '<tr class="form-field">';
    echo '  <th scope="row" valign="top"><label for="cat_pro_featured">' . __('Destaque') . '</label></th>';
    echo '  <td>';
    echo '    <input name="cat_pro_featured" type="checkbox" value="1" ' . checked( $cat_pro_featured, true, false ) . ' />';
    echo '  </td>';
    echo '</tr>';

    ?>

    <tr class="form-field term-group-wrap">
     <th scope="row">
       <label for="category-image-id"><?php _e( 'Imagem', 'walder' ); ?></label>
     </th>
     <td>
       <?php $image_id = get_term_meta ( $term->term_id, 'cat_pro_image', true ); ?>
       <input type="hidden" id="category-image-id" name="category-image-id" value="<?php echo $image_id; ?>">
       <div id="category-image-wrapper">
         <?php if ( $image_id ) { ?>
           <?php echo wp_get_attachment_image ( $image_id, 'thumbnail' ); ?>
         <?php } ?>
       </div>
       <p>
         <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php _e( 'Adicionar Imagem', 'walder' ); ?>" />
         <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php _e( 'Remover Imagem', 'walder' ); ?>" />
       </p>
     </td>
   </tr>

   <?php

	}

	public function save_data( $term_id ) {

		// Destaque da Taxonomia
		$cat_pro_new_featured = isset( $_POST[ 'cat_pro_featured' ] ) ? $_POST[ 'cat_pro_featured' ] : '';
		update_term_meta( $term_id, 'cat_pro_featured', $cat_pro_new_featured );

    // Imagem da taxonomia
    $cat_pro_new_image = isset( $_POST[ 'category-image-id' ] ) ? absint( $_POST[ 'category-image-id' ] ) : '';
    update_term_meta( $term_id, 'cat_pro_image', $cat_pro_new_image );

	}

  public function load_media() {

    // if( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != 'categoria' ) {
    //    return;
    // }

    wp_enqueue_media();

  }

  public function add_script() { ?>
     <script>
       jQuery(document).ready( function($) {
         function ct_media_upload(button_class) {
           var _custom_media = true,
           _orig_send_attachment = wp.media.editor.send.attachment;
           $('body').on('click', button_class, function(e) {
             var button_id = '#'+$(this).attr('id');
             var send_attachment_bkp = wp.media.editor.send.attachment;
             var button = $(button_id);
             _custom_media = true;
             wp.media.editor.send.attachment = function(props, attachment){
               if ( _custom_media ) {
                 $('#category-image-id').val(attachment.id);
                 $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                 $('#category-image-wrapper .custom_media_image').attr('src',attachment.url).css('display','block');
               } else {
                 return _orig_send_attachment.apply( button_id, [props, attachment] );
               }
              }
           wp.media.editor.open(button);
           return false;
         });
       }
       ct_media_upload('.ct_tax_media_button.button');
       $('body').on('click','.ct_tax_media_remove',function(){
         $('#category-image-id').val('');
         $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
       });
       // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-category-ajax-response
       $(document).ajaxComplete(function(event, xhr, settings) {
         var queryStringArr = settings.data.split('&');
         if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
           var xml = xhr.responseXML;
           $response = $(xml).find('term_id').text();
           if($response!=""){
             // Clear the thumb image
             $('#category-image-wrapper').html('');
           }
         }
       });
     });
   </script>
   <?php
  }

}

new Cat_Pro_Term_Meta;
