<?php

function walder_customizer( $wp_customize ){
  /*
  ** PANEL
  */
  $wp_customize->add_panel( 'pan_home_page', array(
   'title' => __( 'Home Page' ),
   'priority' => 21
  ) );

  /*
  **  Cat Products section
  */
  $wp_customize->add_section(
		'sec_cat_products', array(
			'title' => __( 'Categorias em destaque', 'walder'),
			'description' => __( 'Sessão das categorias em destaque', 'walder' ),
      'priority' => 21,
      'panel' => 'pan_home_page',
		)
	);

  // Texto Categorias Destaque
  $wp_customize->add_setting(
		'set_cat_products_html_text', array(
			'type' => 'theme_mod',
			'default' => __( 'Categorias em Destaque', 'walder' ),
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	$wp_customize->add_control(
		'set_cat_products_html_text', array(
			'label' => __( 'Texto das categorias em destaque', 'walder' ),
			'section' => 'sec_cat_products',
			'type' => 'text'
		)
	);


}
add_action( 'customize_register', 'walder_customizer' );

//file input sanitization function
function walder_sanitize_file( $file, $setting ) {

  //allowed file types
  $mimes = array(
      'jpg|jpeg|jpe' => 'image/jpeg',
      'gif'          => 'image/gif',
      'png'          => 'image/png'
  );

  //check file type from file name
  $file_ext = wp_check_filetype( $file, $mimes );

  //if file has a valid mime type return it, otherwise return default
  return ( $file_ext['ext'] ? $file : $setting->default );
}

//checkbox sanitization function
function walder_sanitize_checkbox( $input ){

  //returns true if checkbox is checked
  return ( isset( $input ) && true == $input ? true : false );
}
