<?php

// Post Type Produto
function produto() {

	$labels = array(
		'name'                  => _x( 'Produtos', 'Post Type General Name', 'walder' ),
		'singular_name'         => _x( 'Produto', 'Post Type Singular Name', 'walder' ),
		'menu_name'             => __( 'Produtos', 'walder' ),
		'name_admin_bar'        => __( 'Produtos', 'walder' ),
		'archives'              => __( 'Arquivos do Item', 'walder' ),
		'attributes'            => __( 'Arquivos do Item', 'walder' ),
		'parent_item_colon'     => __( 'Item Pai:', 'walder' ),
		'all_items'             => __( 'Todos os Itens', 'walder' ),
		'add_new_item'          => __( 'Adicionar Novo Item', 'walder' ),
		'add_new'               => __( 'Adicionar Novo', 'walder' ),
		'new_item'              => __( 'Novo Item', 'walder' ),
		'edit_item'             => __( 'Editar Item', 'walder' ),
		'update_item'           => __( 'Atualizar Item', 'walder' ),
		'view_item'             => __( 'Ver Item', 'walder' ),
		'view_items'            => __( 'Ver Itens', 'walder' ),
		'search_items'          => __( 'Buscar Item', 'walder' ),
		'not_found'             => __( 'Não encontrado', 'walder' ),
		'not_found_in_trash'    => __( 'Não encontrado na Lixeira', 'walder' ),
		'featured_image'        => __( 'Imagem destacada', 'walder' ),
		'set_featured_image'    => __( 'Configurar imagem destacada', 'walder' ),
		'remove_featured_image' => __( 'Remover imagem destacada', 'walder' ),
		'use_featured_image'    => __( 'Usar como imagem destacada', 'walder' ),
		'insert_into_item'      => __( 'Inserir no Item', 'walder' ),
		'uploaded_to_this_item' => __( 'Enviado para este item', 'walder' ),
		'items_list'            => __( 'Lista do item', 'walder' ),
		'items_list_navigation' => __( 'Navegação da lista dos itens', 'walder' ),
		'filter_items_list'     => __( 'Filtrar lista de itens', 'walder' ),
	);
	$args = array(
		'label'                 => __( 'Produto', 'walder' ),
		'description'           => __( 'Produtos do site', 'walder' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'categoria' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
    'show_in_rest'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
    'menu_icon'             => 'dashicons-cart',
	);
	register_post_type( 'produto', $args );

}
add_action( 'init', 'produto', 0 );

// Taxonomia Categoria de Produto
function categoria() {

	$labels = array(
		'name'                       => _x( 'Categorias de Produtos', 'Categorias de Produtos', 'walder' ),
		'singular_name'              => _x( 'Categoria', 'Categoria de Produtos', 'walder' ),
		'menu_name'                  => __( 'Categorias de Produtos', 'walder' ),
		'all_items'                  => __( 'Todos os Itens', 'walder' ),
		'parent_item'                => __( 'Item Pai', 'walder' ),
		'parent_item_colon'          => __( 'Item Pai:', 'walder' ),
		'new_item_name'              => __( 'Nome do novo Item', 'walder' ),
		'add_new_item'               => __( 'Adicionar Novo Item', 'walder' ),
    'edit_item'                  => __( 'Editar Item', 'walder' ),
		'update_item'                => __( 'Atualizar Item', 'walder' ),
		'view_item'                  => __( 'Ver Item', 'walder' ),
		'separate_items_with_commas' => __( 'Separar itens com vírgulas', 'walder' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover itens', 'walder' ),
		'choose_from_most_used'      => __( 'Escolher entre os mais usados', 'walder' ),
		'popular_items'              => __( 'Itens Populares', 'walder' ),
		'search_items'               => __( 'Procurar Itens', 'walder' ),
    'not_found'                  => __( 'Não encontrado', 'walder' ),
		'no_terms'                   => __( 'Não há Itens', 'walder' ),
    'items_list'                 => __( 'Lista do item', 'walder' ),
		'items_list_navigation'      => __( 'Navegação da lista dos itens', 'walder' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
    'show_in_rest'               => true,
		'has_archive'                => true,
	);
	register_taxonomy( 'categoria', array( 'produto' ), $args );

}
add_action( 'init', 'categoria', 0 );

function produto_meta_box( $meta_boxes ) {
	$prefix = 'produto-';

	$meta_boxes[] = array(
		'id' => 'geral',
		'title' => esc_html__( 'Informações Gerais', 'walder' ),
		'post_types' => array('produto'),
		'context' => 'side',
		'style' => 'seamless',
		//'default_hidden' => 'false',
		'priority' => 'default',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'featured',
				'name' => esc_html__( 'Destaque', 'walder' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'Produto em destaque na home?', 'walder' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'produto_meta_box' );
