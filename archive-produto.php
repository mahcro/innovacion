<?php get_header(); ?>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <nav aria-label="breadcrumb" class="d-flex justify-content-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?= home_url() ?>">Home</a></li>
          <li class="breadcrumb-item">Productos</li>
        </ol>
      </nav>
      <div class="row page-top">
        <div class="col-md-6 mx-auto">
          <h1>Productos</h1>
          <p class="text-muted">Aquí encontrará todos nuestros productos, si lo prefiere puede ver separados por categorías.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- /row -->
  <div class="row">
    <div class="col-lg-3">
      <!-- menus and filters-->
      <div class="card card-default sidebar-menu">
        <div class="card-heading">
          <h5 class="card-title">Categorías</h5>
        </div>
        <div class="card-body">
          <?php $wcatTerms = get_terms('categoria', array('hide_empty' => false, 'parent' => false));
             foreach($wcatTerms as $wcatTerm) :
          ?>
          <ul class="nav nav-pills flex-column category-menu">
            <li><a href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>" class="nav-link d-flex align-items-center justify-content-between">
              <?php echo $wcatTerm->name; ?> <span class="badge badge-secondary pull-right">
                <?php echo wp_get_postcount($wcatTerm->term_id, 'categoria'); ?>
              </span></a>
              <ul>
                <?php
                   $wsubargs = array(
                      'hierarchical' => 1,
                      'show_option_none' => '',
                      'hide_empty' => false,
                      'parent' => $wcatTerm->term_id,
                      'taxonomy' => 'categoria'
                   );
                   $wsubcats = get_categories($wsubargs);
                   foreach ($wsubcats as $wsc):
                   ?>
                <li> <a href="<?php echo get_term_link( $wsc->slug, $wsc->taxonomy );?>"><?php echo $wsc->name;?></a></li>
                <?php
                   endforeach;
                   ?>
              </ul>
            </li>
          </ul>
          <?php
             endforeach;
             ?>
        </div>
      </div>
    </div>
    <!-- /col-lg-3 -->
    <div class="col-lg-9">
      <?php

        $args = [
          'post_type' => 'produto',
          'posts_per_page' => 9,
          'order'      => 'DESC',
          'orderby'    => 'id',
          'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
        ];
    		$loop = new WP_Query( $args );

      ?>

    	<?php	if( $loop->have_posts() ): ?>
      <div class="row products">
      <?php
        while( $loop->have_posts() ):
    		$loop->the_post();
      ?>
        <!-- product-->
        <div class="col-lg-4 col-md-6">
          <div class="product">
            <div class="image">

              <a href="<?php the_permalink(); ?>">
                <img src="<?=
                has_post_thumbnail() ?
                the_post_thumbnail_url('produto') :
                get_template_directory_uri() . '/img/produto-sem-imagem.jpg';
                ?>" class="img-fluid" />
              </a>
            </div>
            <div class="text">
              <h3> <a href="<?php the_permalink(); ?>"><?= get_the_title() ?></a></h3>
              <button style="margin: 1em;" onclick="addToCart(<?php the_ID(); ?>);" class="btn btn-primary product-<?php the_ID(); ?>">Presupuesto</button>
            </div>
          </div>
        </div>
        <!-- /product-->
      <?php endwhile; ?>
      </div>

      <div class="pagination d-flex justify-content-center mb-5 category-pagination" >
        <?php

        $big = 999999999; // need an unlikely integer
        echo paginate_links(
          array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $loop->max_num_pages,
            'prev_text'          => __('«'),
          	'next_text'          => __('»'),
        ) );
        ?>
      </div>
      <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<!-- /container -->

<?php get_footer(); ?>
