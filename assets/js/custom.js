function addToCart( id ){

  // pega conteudo do botão
  var botao = jQuery('.product-'+id).html();
  jQuery('.product-'+id).html('<b>Esperar... <span class="fa fa-cog fa-spin"></span></b>');

  var data = {
    'action': 'addToCart',
    'id': id,
  }
  jQuery.post(ajax_object.ajax_url, data, function(response) {

    // resposta no console
    console.log(response);
    // resposta no modal e abrir modal
    jQuery('#addProduct-modal').modal('toggle');
    // recoloca conteudo do botão
    jQuery('.product-'+id).html(botao);
  });

}

function clearCart(){

  var botao = jQuery("#btnClearCart").html();
  jQuery("#btnClearCart").html('<b>Esperar... <i class="fa fa-cog fa-spin"></i></b>');

  var data = {'action': 'clearCart'}

  jQuery.post(ajax_object.ajax_url, data, function(response) {
    console.log(response);

    jQuery('#retorno').html(response);

    // recoloca conteudo do botão
    jQuery('#btnClearCart').html(botao);
  });

}

function removeFromCart( id ){

  jQuery('#exclui' + id).html('<i class="fa fa-cog fa-spin"></i>');

  var data = {
    'action': 'removeFromCart',
    'id': id,
  }

  jQuery.post(ajax_object.ajax_url, data, function(response) {

    console.log(response);

    jQuery('#retorno').html(response);

  });

}

function saveToDB(productId)
{
    console.log("saving..");
    var productQt2 = jQuery('input#qt' + productId).val();

    var data = {
      'action': 'modifyCart',
      'id': productId,
      'qtd': productQt2,
    }

    jQuery.post(ajax_object.ajax_url, data, function(response) {

      console.log(response);

      jQuery('#retorno').html(response);

    });
}
