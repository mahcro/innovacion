<?php get_header(); ?>

<div id="contact" class="container">
  <div class="row">
    <div class="col-lg-12">
      <nav aria-label="breadcrumb" class="d-flex justify-content-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?= home_url() ?>">Home</a></li>
          <li aria-current="page" class="breadcrumb-item active">Contacto</li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row mb-5 text-center">
    <div class="col-lg-10 mx-auto">
      <div class="heading">
        <h2>ESTAMOS AQUÍ PARA AYUDARTE</h2>
      </div>
      <p class="lead text-center">
        Póngase en contacto y conozca los mejores productos para nutrición y salud animal.
      </p>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-6">
      <div class="box-simple">
        <div class="icon"><i class="fa fa-map-marker"></i></div>
        <h3>Dirección</h3>
        <p>
          <strong>Pedro Juan Caballero - Departamento de Amambay</strong><br>
          Calle Carlos Antonio Lopez, n. 3720, Esquina Boquerón<br>
          <strong>Horario de atención:</strong><br>
          Lunes a Viernes de las 07:00 hasta 11:30h y de las 13:00 hasta 17:30h<br>
          Sabados de las 07:00 hasta 11:30h
        </p>
        <p>
          <strong>Sucursal Mariano Roque Alonso - Departamento Central</strong><br>
          Calle No 9, casi Bernardino Caballero<br>
          <strong>Horario de atención:</strong><br>
          Lunes a Viernes de las 08:00 hasta las 12:00h y de las 13:30 hasta 18:00h<br>
          Sabados de las 08:00 hasta 12:00h
        </p>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="box-simple">
        <div class="icon"><i class="fa fa-phone"></i></div>
        <h3>CENTRO DE LLAMADAS</h3>
        <p><strong>Pedro Juan Caballero</strong><br>+595 336 270 004</p>
        <p><strong>Sucursal Mariano Roque Alonso</strong><br>+595 217 289 609</p>
      </div>
      <div class="box-simple">
        <div class="icon"><i class="fa fa-envelope"></i></div>
        <h3>E-mail</h3>
        <p class="text-muted">Por favor, siéntase libre de escribir un correo electrónico.</p>
        <ul class="list-unstyled">
          <li><strong><a href="mailto:">atencionalcliente@innovacionsrl.net</a></strong></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row text-center mb-5">
    <div class="col-lg-12">
      <div class="heading">
        <h2>Formulario de contacto</h2>
      </div>
    </div>
    <div class="col-lg-8 mx-auto">
      <?= do_shortcode('[contact-form-7 title="Formulário Contato"]') ?>
    </div>
  </div>
</div>
<div class="container-fluid">
<div class="row">
  <div class="col-lg-6">
    <h4 class="text-center">Pedro Juan Caballero</h4>
    <!-- Google Map -->
    <div class="map-wrap">
      <div id="maps" style="height: 400px;">
        <iframe
    		  width="100%"
    		  height="400"
    		  frameborder="0" style="border:0"
    		  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14738.016969759705!2d-55.7197109!3d-22.5602366!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94626e85b69a4df3%3A0xef7256edcdbe7fd!2sInnovaci%C3%B3n%20S.R.L.%20-%20Real%20H!5e0!3m2!1spt-BR!2sbr!4v1598283243147!5m2!1spt-BR!2sbr" allowfullscreen>
    		</iframe>
      </div>
    </div>
    <!-- /Google Map -->
  </div>
  <div class="col-lg-6">
    <h4 class="text-center">Asunción</h4>
    <!-- Google Map -->
    <div class="map-wrap">
      <div id="maps" style="height: 400px;">
        <iframe
    		  width="100%"
    		  height="400"
    		  frameborder="0" style="border:0"
    		  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3609.766675317292!2d-57.51422948499014!3d-25.21108988388977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da559e622e543%3A0xa4eb094608fb344a!2sInnovaci%C3%B3n%20SRL%20-%20Real%20H!5e0!3m2!1spt-BR!2sbr!4v1598283301581!5m2!1spt-BR!2sbr" allowfullscreen>
    		</iframe>
      </div>
    </div>
    <!-- /Google Map -->
  </div>
</div>
</div>


<?php get_footer(); ?>
