<form role="search" method="get" action="<?php echo home_url('/'); ?>">
  <div class="input-group"><span class="input-group-btn">
      <button type="submit" class="btn"><i class="fa fa-search"></i></button></span>
    <input name="s" type="search" placeholder="BUSCA" class="form-control" value="<?php echo get_search_query(); ?>">
  </div>
</form>
