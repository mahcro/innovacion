<?php

get_header(); ?>

<div class="parea-inner-content">
	<main>
		<section class="parea-content">
			<div class="container">
				<div class="row">

					<div class="error404 col-md-9">

					<header>
						<h1>Página no encontrada</h1>
						<p>Desafortunadamente, la página a la que intentaste acceder no existe en este sitio</p>
					</header>

					


					</div>
				</div>
			</div>
		</section>

	</main>
</div>
<?php get_footer(); ?>
