<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

  <div class="topbar">
    <div class="container">
      <div class="topbar__content row">
        <!-- Busca -->
        <div class="col-md-3 ml-auto d-md-block d-none topbar__search">
					<?php get_search_form(); ?>
        </div>

        <!-- Logo -->
        <div class="col-md-4 topbar__logo">
          <a href="<?= home_url() ?>">
            <img src="<?php echo get_template_directory_uri() . '/img/logo.png' ?>" alt="" class="topbar__logo__normal d-none d-md-inline-block">
            <img src="<?php echo get_template_directory_uri() . '/img/logo-small.png'?>" alt="" class="topbar__logo__small d-inline-block d-md-none">
          </a>
        </div>

        <!-- Carrinho -->
        <div class="col-md-3 ml-auto d-md-block d-none">
          <div class="topbar__cart">
						<a href="/presupuesto" class="btn btn-outline-dark"><i class="fa fa-shopping-cart"></i>
							<span>PRESUPUESTO</span>
						</a>
					</div>
        </div>
      </div>
    </div>
  </div>

  <?php if (is_front_page()): ?>
    <?= do_shortcode("[slide-anything id=26]"); ?>
  <?php endif; ?>

  <nav class="navbar navbar-expand-lg">
    <div class="container d-flex justify-content-center">

      <button type="button" data-toggle="collapse" data-target=".collapsed-search" class="navbar-toggler btn btn-outline-dark">
        <i class="fa fa-search"></i>
        <span class="sr-only">Toggle navigation</span>
      </button>

      <a href="basket.html" class="navbar-toggler btn btn-outline-dark">
        <i class="fa fa-shopping-cart"></i>
        <span class="d-none d-lg-block">3 Items in cart</span>
      </a>

      <button type="button" data-toggle="collapse" data-target="#navigation" class="navbar-toggler btn btn-outline-dark">
        <i class="fa fa-align-justify"></i>
        <span class="sr-only">Toggle navigation</span>
      </button>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div id="navigation" class="collapse navbar-collapse">
						<?php wp_nav_menu( [
              'theme_location' => 'my_main_menu',
              'menu_class' => 'nav navbar-nav mx-auto',
              'container'=> false,
							'add_li_class'  => 'nav-link'
              ] ); ?>
      </div>
      <!-- /.Navbar-collapse -->

      <div class="collapsed-search collapse">
				<?php get_search_form(); ?>        
      </div>

    </div>
  </nav>
