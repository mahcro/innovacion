<div id="retorno"></div>
<!-- modal orçamento - produto adicionado -->
<div id="addProduct-modal" tabindex="-1" role="dialog" aria-hidden="false" class="modal fade">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Producto agregado al presupuesto!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Usted puede visitar la página de presupuesto o continuar mirando nuestros productos.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Continuar presupuesto</button>
        <a href="/presupuesto" class="btn btn-primary">Listar presupuesto</a>
      </div>
    </div>
  </div>
  <!-- /.modal-dialog-->
</div>
<!-- /.modal-->
<!-- /modal orçamento - produto adicionado -->

<footer class="footer">
  <div class="footer__block">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6">
          <h4>Hable con nosotros</h4>
          <?php if( is_active_sidebar( 'sidebar-3' ) ): ?>
              <?php dynamic_sidebar( 'sidebar-3' ); ?>
          <?php endif; ?>
        </div>
        <div class="col-lg-6 col-md-6">
          <?php get_sidebar( 'facebook' ); ?>
        </div>
      </div>
      <div class="row">

        <div class="col-lg-6 col-md-6">          
            <?php if( is_active_sidebar( 'sidebar-2' ) ): ?>
            		<?php dynamic_sidebar( 'sidebar-2' ); ?>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-center text-md-left">
          <p>&copy;<?= ("2019" == date("Y"))? "2019"  : "2019 - " . date("Y"); ?> Innovación S.R.L.
        </div>
        <div class="col-md-6 text-center text-md-right mt-3 mt-md-0">
          <p class="credit">Desarrollado por <a href="http://agenciamorfose.com.br/">Agência Morfose</a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
