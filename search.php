<?php get_header();  ?>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <nav aria-label="breadcrumb" class="d-flex justify-content-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?= home_url() ?>">Home</a></li>
          <li class="breadcrumb-item">Resultados de búsqueda</li>
        </ol>
      </nav>
      <div class="row page-top">
        <div class="col-md-10 mx-auto">
          <h1>Resultados de búsqueda para <?php echo get_search_query(); ?></h1>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-9">
      <?php	if( have_posts() ): ?>
      <div class="row products">
      <?php
        while( have_posts() ):
    		the_post();
      ?>
        <!-- product-->
        <div class="col-lg-4 col-md-6">
          <div class="product">
            <div class="image">

              <a href="<?php the_permalink(); ?>">
                <img src="<?=
                has_post_thumbnail() ?
                the_post_thumbnail_url('produto') :
                get_template_directory_uri() . '/img/produto-sem-imagem.jpg';
                ?>" class="img-fluid" />
              </a>
            </div>
            <div class="text">
              <h3> <a href="<?php the_permalink(); ?>"><?= get_the_title() ?></a></h3>
            </div>
          </div>
        </div>
        <!-- /product-->
      <?php endwhile; ?>
      </div>
    <?php else: ?>
      <p class="text-muted">Ningún resultado encontrado</p>
    <?php endif; ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>
