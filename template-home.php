<?php
/**
 * Template Name: Home Page
 *
 * Description: A custom page template for displaying a home page
 *
 * @package WordPress
 * @subpackage Walder
 * @since 1.0
 * @version 1.0
 */
get_header();

?>

<div class="container">

  <?php

    // Categorias em destaque
    $args = [
      'taxonomy'   => 'categoria',
      'number'     => 2,
      'order'      => 'DESC',
      'orderby'    => 'id',
      'meta_key'   => 'cat_pro_featured',
      'meta_value' => '1',
      'hide_empty' => false,
    ];
    $itens = get_terms( $args );

  ?>

  <?php if ( ! empty( $itens ) && ! is_wp_error( $itens ) ): ?>
  <!-- Sessão de categorias em destaque -->
  <section>
    <div class="row">
      <div class="col-md-12">
        <div class="heading">
          <h4><?php echo get_theme_mod( 'set_cat_products_html_text' ); ?></h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <div class="row">
    			<?php
            foreach($itens as $cat):
              // Get the image ID for the category
              $image_id  = get_term_meta( $cat->term_id, 'cat_pro_image', true );
              $image_url = wp_get_attachment_url( $image_id ) ? wp_get_attachment_url( $image_id ) : get_template_directory_uri() . '/img/sem-foto-categoria.jpg';
              // echo wp_get_attachment_image ( $image_id, 'large' ); para imprimir a imagem com tags <img>
              $term_link = get_term_link( $cat->term_id );
          ?>
          <div class="col-md-6">
             <a href="<?= esc_url( $term_link ) ?>" class="margin-bottom">
               <img src="<?= $image_url ?>" alt="" class="img-fluid img-border">
             </a>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
  <?php endif; ?>

  <?php

    $args = [
      'post_type' => 'produto',
      'posts_per_page' => 8,
      'meta_key'   => 'produto-featured',
      'meta_value' => '1',
      'order'      => 'DESC',
      'orderby'    => 'id',
    ];
		$loop = new WP_Query( $args );

  ?>

	<?php	if( $loop->have_posts() ): ?>

  <section>
    <div class="row">
      <div class="col-md-12">
        <div class="heading">
          <h4>Destacados</h4>
        </div>
      </div>
    </div>
    <div class="row products">

	<?php
    while( $loop->have_posts() ):
		$loop->the_post();
  ?>
    <div class="col-md-3 col-6">
      <div class="product">
        <div class="image">
          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('produto', array('class' => 'img-fluid')); ?></a>
        </div>
        <div class="text">
          <h3> <a href="<?php the_permalink(); ?>"><?= get_the_title() ?></a></h3>
          <button style="margin: 1em;" onclick="addToCart(<?php the_ID(); ?>);" class="btn btn-primary product-<?php the_ID(); ?>">Presupuesto</button>
        </div>
      </div>
    </div>
		<?php
			endwhile;
			wp_reset_postdata();
			endif;
		?>

    </div>
  </section>
</div>



<?php get_footer(); ?>
